﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021_S10208226.Models
{
    public class Vote
    {
        public int BookId { get; set; }
        public string Justification { get; set; }
    }
}
