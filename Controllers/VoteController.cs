﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB2021_S10208226.DAL;
using WEB2021_S10208226.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace WEB2021_S10208226.Controllers
{
    public class VoteController : Controller
    {
        // GET: VoteController
        public ActionResult Index()
        {
            return View();
        }

        // GET: VoteController/Details/5
        public async Task<ActionResult> Details(int id)
        {
            // Make Web API call to get a list of votes related to a BookId
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://mstudentproject.azurewebsites.net");
            // Note: the "id" here is the BookId
            HttpResponseMessage response = await
            client.GetAsync("/api/votes/" + id.ToString());
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                List<VoteDetails> voteList =
                 JsonConvert.DeserializeObject<List<VoteDetails>>(data);
                return View(voteList);
            }
            else
            {
                return View(new List<VoteDetails>());
            }
        }

        // GET: VoteController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VoteController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(IFormCollection collection)
        {
            // Read BookId and Justification from HTML Form
            int bookid = Convert.ToInt32(collection["item.Id"]);
            string justification = collection["item.Justification"];

            // Transfer data read to a vote object
            Vote vote = new Vote();
            vote.BookId = bookid;
            vote.Justification = justification;

            // Make Web API call to post the vote object 
            HttpClient client = new HttpClient();
            client.BaseAddress = new
             Uri("https://mstudentproject.azurewebsites.net");
            //Convert the vote to JSON string
            string json = JsonConvert.SerializeObject(vote);
            StringContent votecontent = new StringContent(json, UnicodeEncoding.UTF8,
           "application/json");
            HttpResponseMessage response = await client.PostAsync("/api/votes",
            votecontent);
            if (response.StatusCode == HttpStatusCode.Created)
            {
                // Successful – code 201 returned
                return RedirectToAction("Details", new { id = bookid });
            }
            else
            {
                // Unsuccessful – other returned code
                TempData["BookId"] = bookid;
                TempData["Justification"] = justification;
                TempData["Message"] = "Fail to add vote record!";
                return RedirectToAction("Index", "Book");
            }
        }

        // GET: VoteController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: VoteController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: VoteController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: VoteController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
